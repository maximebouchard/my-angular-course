import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../services/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  authStatus: boolean;

  constructor(private authentificationService: AuthentificationService, private router: Router) { }

  ngOnInit() {
    this.authStatus = this.authentificationService.isAuth;
  }

  onSignIn(){
    this.authentificationService.signIn().then(
      () => {
        console.log("Vous êtes connecté");
        this.authStatus = this.authentificationService.isAuth;
        this.router.navigate(['appareils']);
      }
    )
  }

  onSignOut(){
    this.authentificationService.signOut();
    console.log("Vous êtes déconnecté");
    this.authStatus = this.authentificationService.isAuth;
  }

}
