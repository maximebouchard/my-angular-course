import { Subject } from 'rxjs';

export class AppareilService {

  appareilsSubject = new Subject<any[]>();

  private secondAppareillage = [
    {
      id: 1,
      name: 'Grille-pain',
      status: 'éteint'
    },
    {
      id: 2,
      name: 'Réfrigérateur',
      status: 'allumé'
    },
    {
      id: 3,
      name: 'Ampoule',
      status: 'allumé'
    }
  ];

  emitAppareilSubject() {
    this.appareilsSubject.next(this.secondAppareillage.slice());
  }

  getAppareilById(id: number){
    const appareil = this.secondAppareillage.find(
      (object) => {
        return object.id === id;
      }
    );
    return appareil;
  }

  switchOnAll() {
    for (let appareil of this.secondAppareillage) {
      appareil.status = "allumé";
    }
    this.emitAppareilSubject();
  }

  swithOffAll() {
    for (let appareil of this.secondAppareillage) {
      appareil.status = "éteint";
    }
    this.emitAppareilSubject();
  }

  switchOnOne(index: number) {
    this.secondAppareillage[index].status = "allumé";
    this.emitAppareilSubject();
  }

  switchOffOne(index: number) {
    this.secondAppareillage[index].status = "éteint";
    this.emitAppareilSubject();
  }

  addAppareil(name: string, status: string){
    const appareilObject = {
      id: 0,
      name: '',
      status: ''
    };
    appareilObject.name = name;
    appareilObject.status = status;
    appareilObject.id = this.secondAppareillage[(this.secondAppareillage.length - 1)].id + 1;
    this.secondAppareillage.push(appareilObject);
    this.emitAppareilSubject();
  }

}
