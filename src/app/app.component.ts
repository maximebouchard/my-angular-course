import { Component, OnInit, OnDestroy } from '@angular/core'; //implement natives interfaces
import { interval, Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  secondes: number;
  counterSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    const counter = interval(1000); //instanciate an observable
    this.counterSubscription = counter.subscribe( //listen the observable stocked in the Subrscription object
      (value) => {
        this.secondes = value;
      },
      (error) => {
        console.log("une erreure est survenue : " + error);
      },
      () => {
        console.log("Observable complete");
      }
    )
  }

  ngOnDestroy() {
    this.counterSubscription.unsubscribe(); //prevents infinite errors
  }
}
